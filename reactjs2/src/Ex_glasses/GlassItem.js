import React, { Component } from "react";

export default class GlassItem extends Component {
  render() {
    let { url } = this.props.data;
    return (
      <div className="col-3 py-3">
        <div>
          <img
            style={{ width: "100%" }}
            src={url}
            alt=""
            onClick={() => {
              this.props.handleChangeDetail(this.props.data);
            }}
          />
        </div>
      </div>
    );
  }
}
