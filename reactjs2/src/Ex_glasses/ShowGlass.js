import React, { Component } from "react";

export default class ShowGlass extends Component {
  render() {
    return (
      <div className="detail">
        <img src={this.props.detail.url} alt="" className="glass-img" />
        <div className="detail__content bg-dark">
          <p className="detail__name text-white">{this.props.detail.name}</p>
          <p className="text-white">{this.props.detail.desc}</p>
        </div>
      </div>
    );
  }
}
