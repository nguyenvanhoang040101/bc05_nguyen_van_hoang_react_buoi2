import React, { Component } from "react";
import dataGlass from "../dataGlasses.json";
import GlassList from "./GlassList";
import ShowGlass from "./ShowGlass";

export default class Ex_glasses extends Component {
  state = {
    dataGlass,
    content: dataGlass[0],
  };
  handleChangeDetail = (value) => {
    this.setState({ content: value });
  };
  render() {
    return (
      <div className="container">
        <div className="title py-4 mx-auto bg-dark text-white display-4">
          The Glasses Online
        </div>
        <div className="row justify-content-center align-items-center">
          <img
            className="col-5 mr-5"
            style={{ width: "200px", height: "500px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
          <img
            className="col-5 ml-5  position-relative"
            style={{ width: "200px", height: "500px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
        </div>
        <GlassList
          handleChangeDetail={this.handleChangeDetail}
          glassArr={this.state.dataGlass}
        />
        <ShowGlass detail={this.state.content} />
      </div>
    );
  }
}
